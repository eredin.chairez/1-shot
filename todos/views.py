from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem

# Create your views here.


def show_items(request, id):
    todo = get_object_or_404(TodoItem, id=id)
    context = {
        "todo_item": todo,
    }
    return render(request, "todos/detail.html", context)


def show_todos(request):
    todos = TodoList.objects.all()
    context = {
        "todos_list": todos
    }
    return render(request, "todos/list.html", context)
