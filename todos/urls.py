from django.urls import path
from todos.views import show_todos, show_items


urlpatterns = [
    path("<int:id>/", show_items, name="todos_list_detail"),
    path("", show_todos, name="todos_list_list"),
]
